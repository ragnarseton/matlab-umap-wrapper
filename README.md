# MATLAB UMAP wrapper
MATLAB wrapper class for the python reference implementation of [UMAP](https://github.com/lmcinnes/umap). Tested with R2018b, R2023b (and some in between) and various versions of python on macos. Also R2023b and python 3.11.3 on linux.

utils.m is not usable in its current state.

## Installation
Easiest way is to just download [umap.m](https://gitlab.com/ragnarseton/matlab-umap-wrapper/-/raw/master/umap.m?ref_type=heads&inline=false) to your ~/Documents/MATLAB folder and you should be able to just call it straight from matlab.

If you want to be able to track the latest version of it (hehehe, right) you can clone the repo like so
```bash
git clone https://gitlab.com/ragnarseton/matlab-umap-wrapper.git ~/Documents/MATLAB/@umap
```

### Matlab/Python setup
Basically all you have to do is install umap, which can be done with a package manager or like this with pip (the following should work in macos):
```bash
pip install umap-learn
```
If you don't want to install it system wide but use it in an environment see next section.

Next make sure python is set up in matlab, this is well documented in mathworks's [docs](https://se.mathworks.com/help/matlab/matlab_external/install-supported-python-implementation.html) but essentially boils down to running `pyenv()` in matlab and if it comes up empty you run it with the absolute path to the python3 executable (you can get it by running `which python3` in the terminal) `pyenv(Version='/absolute/path/to/python3')`.

### Linux/matlab/virtual python env setup
While this should work on macos as well it (afaik) ships with matlab-compatible python versions so there is no need for virtual environments but if you want one go right ahead. The code below assumes you're using come kind of package manager. There are plenty of resources about how to do this so i'll just dump this here (assumes [micromamba](https://mamba.readthedocs.io/en/latest/installation/micromamba-installation.html) but might work with conda or mamba or similar, i don't know tho).
```bash
PKG_MNGR=micromamba
MATLAB_ENV=matlab_env

# create environment (-y means just go ahead without confirmation, -n is name)
$PKG_MNGR create -y -n "$MATLAB_ENV" python=3.11.3
# install umap
$PKG_MNGR install -y -n "$MATLAB_ENV" -c conda-forge umap-learn

# activate to determine executable
$PKG_MNGR activate "$MATLAB_ENV"
python
>>> import sys
>>> sys.executable
# output should be something like
# '/home/username/pkg_mngr/envs/matlab_env/bin/python'
>>> exit()

# deactivate
$PKG_MNGR deactivate
```

then in matlab you run (replacing the `<sys.executable.output>` of course)
```matlab
pyenv('Version', '<sys.executable output>', 'ExecutionMode', 'OutOfProcess')
```

## TODO
- add examples
- finish utils.m
- check if anything was updated in the python package over the last half decade =)
- the stuff that's in umap.m

## Docs
Pretty much all copied from the official ones (back in 2018/19).

See umap.m (or if you've downloaded already using `doc umap` in matlab).

## About the License
Since i for the vast majority of the documentation pretty much copied the
official one i went with the same license..


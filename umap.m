classdef umap < handle
% Uniform Manifold Approximation and Projection for Dimension Reduction (UMAP),
% python reference implementation wrapper class.
% For more info see [1,2] or [3] for the props and functions.
%
% About
%	I've tried to make it sorta MATLABy, meaning that integer properties are
%	returned as doubles when get()ed and can be set using any numeric class
% 	(validated by umap.num_classes). Input validation is also performed on the
% 	MATLAB side of things too avoid to much overhead.
%
% Requirements
%	- MATLAB compatible python3 version installed
%	- umap-learn
%
% Documentation
%	The docs on the different props and functions here are mostly copied from
%	[3], only changes are those where data types differ. For more in-depth info
%	see [1-3].
%
% Notes
%	- In case you want to fiddle with the UMAP python object directly it is
%	  available through the py_obj property.
%	- Instead of setting an explicit None value here anything that when given as
%	  an argument to isempty evaluates to true is treated as None.
%	- When a None value is to be returned [] is used.
%	- Python dictionaries, i.e. values for *_kwds-properties, can be given as
%	  either matlab structs (closer to python structure) or key-value cell
%	  arrays (closer to python syntax), e.g.:
%		% these are both equivalent to the python code
%		% o.metric_kwds = {'a': 1, 'b', 'test string'}
%		o.metric_kwds = struct('a', 12, 'b', 'test string');
%		o.metric_kwds = {'a', 12, 'b', 'test string'};
%	  They are however always returned as structs (or [] in case of
%	  py.None-values).
%
% Shortcommings
%	- Only numeric target arrays (y in fit(X, y) and fit_transform(X, y)) are
%	  supported as of now.
%	- Custom metric functions (callables) is not yet implemented.
%	- RandomState instance not supported as value for random_state.
%
% Extensions
%   I've added a rudimentary visualization function that supports coloring based
%   on labels and custom label names in the legend, umap.plot.
%
% TODO
%	- Fix the shortcommings
%	- Write test that sets all values and then assert them equal to py_objs's
%	- Test with other versions of MATLAB
%
% Contact/bug reports
%   online: https://gitlab.com/ragnarseton/matlab-umap-wrapper
%   email:  ragnar.seton@uit.no
%
% Refs
%	[1] https://umap-learn.readthedocs.io/en/latest/index.html
%	[2] https://joss.theoj.org/papers/2dd7bee9c9ae58ada787bde947592605
%	[3] https://umap-learn.readthedocs.io/en/latest/api.html

	properties (Constant = true)
% Valid values for the metric property
%
% See also metric
		metrics = {	'euclidean', 'l2', 'manhattan', 'taxicab', 'l1', ...
					'chebyshev', 'linfinity', 'linfty', 'linf', 'minkowski', ...
					'seuclidean', 'standardised_euclidean', 'wminkowski', ...
					'weighted_minkowski', 'mahalanobis', 'canberra', ...
					'cosine', 'correlation', 'haversine', 'braycurtis', ...
					'hamming', 'jaccard', 'dice', 'matching', 'kulsinski', ...
					'rogerstanimoto', 'russellrao', 'sokalsneath', ...
					'sokalmichener', 'yule' };
% Valid values for the target_metric property
%
% See also target_metric
		target_metrics = [umap.metrics(:); {'categorical'}];
% Valid char-values for the init property
%
% See also init
		inits = { 'spectral', 'random' };
% Classes that are accepted for numeric props
		num_classes = {	'single', 'double', ...
						'int8', 'int16', 'int32', 'int64', ...
						'uint8', 'uint16', 'uint32', 'uint64' };
	end

	properties (Dependent = true)
% The size of local neighborhood (in terms of number of neighboring sample
% points) used for manifold approximation. Larger values result in more global
% views of the manifold, while smaller values result in more local data being
% preserved. In general values should be in the range 2 to 100.
% Value range: integer > 0
% Default: 15 (fairly local)
		n_neighbors;
% The dimension of the space to embed into. This defaults to 2 to provide easy
% visualization, but can reasonably be set to any integer value in the range 2
% to 100.
% Value range: integer > 0
% Default: 2
		n_components;
% The metric to use to compute distances in high dimensional space.  The ones
% that take arguments (such as minkowski, mahalanobis etc.) can have arguments
% passed via the metric_kwds dictionary. At this time care must be taken and
% dictionary elements must be ordered appropriately; this will hopefully be
% fixed in the future.
% Value range: 'euclidean' | 'l2' | 'manhattan' | 'taxicab' | 'l1' |
%	'chebyshev' | 'linfinity' | 'linfty' | 'linf' | 'minkowski' | 'seuclidean' |
%	'standardised_euclidean' | 'wminkowski' | 'weighted_minkowski' |
%	'mahalanobis' | 'canberra' | 'cosine' | 'correlation' | 'haversine' |
%	'braycurtis' | 'hamming' | 'jaccard' | 'dice' | 'matching' | 'kulsinski' |
%	'rogerstanimoto' | 'russellrao' | 'sokalsneath' | 'sokalmichener' | 'yule'
% Default: 'euclidean'
		metric;
% The number of training epochs to be used in optimizing the low dimensional
% embedding. Larger values result in more accurate embeddings. If [] is
% specified a value will be selected based on the size of the input dataset (200
% for large datasets, 500 for small).
% Value range: integer > 0 or []
% Default: []
		n_epochs;
% The initial learning rate for the embedding optimization.
% Value range: float > 0
% Default: 1.0
		learning_rate;
% How to initialize the low dimensional embedding.
% Value range:
%	'spectral': use a spectral embedding of the fuzzy 1-skeleton
%	'random': assign initial embedding positions at random.
%	An array of initial embedding positions.
% Default: 'spectral'
		init;
% The effective minimum distance between embedded points. Smaller values will
% result in a more clustered/clumped embedding where nearby points on the
% manifold are drawn closer together, while larger values will result on a more
% even dispersal of points. The value should be set relative to the spread
% value, which determines the scale at which embedded points will be spread out.
% Value range: float >= 0
% Default: 0.1
%
% See also spread
		min_dist;
% The effective scale of embedded points. In combination with min_dist this
% determines how clustered/clumped the embedded points are.
% Value range: float >= 0
% Default: 1.0
%
% See also min_dist
		spread;
% Interpolate between (fuzzy) union and intersection as the set operation used
% to combine local fuzzy simplicial sets to obtain a global fuzzy simplicial
% sets. Both fuzzy set operations use the product t-norm. The value of this
% parameter should be between 0.0 and 1.0; a value of 1.0 will use a pure fuzzy
% union, while 0.0 will use a pure fuzzy intersection.
% Value range: 0 <= float <= 1
% Default: 1.0
		set_op_mix_ratio;
% The local connectivity required – i.e. the number of nearest neighbors that
% should be assumed to be connected at a local level. The higher this value the
% more connected the manifold becomes locally. In practice this should be not
% more than the local intrinsic dimension of the manifold.
% Value range: integer >= 0
% Default: 1
		local_connectivity;
% Weighting applied to negative samples in low dimensional embedding
% optimization. Values higher than one will result in greater weight being given
% to negative samples.
% Value range: float
% Default: 1.0
		repulsion_strength;
% The number of negative samples to select per positive sample in the
% optimization process. Increasing this value will result in greater repulsive
% force being applied, greater optimization cost, but slightly more accuracy.
% Value range: integer
% Default: 5
		negative_sample_rate;
% For transform operations (embedding new points using a trained model_ this
% will control how aggressively to search for nearest neighbors. Larger values
% will result in slower performance but more accurate nearest neighbor
% evaluation.
% Value range: float
% Default: 4.0
		transform_queue_size;
% More specific parameters controlling the embedding. If [] these values are set
% automatically as determined by min_dist and spread.
% Value range: float or []
% Default: []
%
% See also min_dist, spread
		a;
% More specific parameters controlling the embedding. If [] these values are set
% automatically as determined by min_dist and spread.
% Value range: float or []
% Default: []
%
% See also min_dist, spread
		b;
% If int, random_state is the seed used by the random number generator; If [],
% the random number generator is the RandomState instance used by np.random.
% Value range: integer or []
% Default: []
		random_state;
% Arguments to pass on to the metric, such as the p value for Minkowski
% distance. If [] then no arguments are passed on.
% Value range: struct/key-value cell array or []
% Default: []
		metric_kwds;
% Whether to use an angular random projection forest to initialise the
% approximate nearest neighbor search. This can be faster, but is mostly on
% useful for metric that use an angular style distance such as cosine,
% correlation etc. In the case of those metrics angular forests will be chosen
% automatically.
% Value range: bool
% Default: false
		angular_rp_forest;
% The number of nearest neighbors to use to construct the target simplcial set.
% If set to -1 use the n_neighbors value.
% Value range: integer >= -1
% Default: -1
%
% See also n_neighbors
		target_n_neighbors;
% The metric used to measure distance for a target array is using supervised
% dimension reduction. By default this is ‘categorical’ which will measure
% distance in terms of whether categories match or are different. Furthermore,
% if semi-supervised is required target values of -1 will be trated as
% unlabelled under the ‘categorical’ metric. If the target array takes
% continuous values (e.g. for a regression problem) then metric of ‘l1’ or ‘l2’
% is probably more appropriate.
% Value range: 'euclidean' | 'l2' | 'manhattan' | 'taxicab' | 'l1' |
%	'chebyshev' | 'linfinity' | 'linfty' | 'linf' | 'minkowski' | 'seuclidean' |
%	'standardised_euclidean' | 'wminkowski' | 'weighted_minkowski' |
%	'mahalanobis' | 'canberra' | 'cosine' | 'correlation' | 'haversine' |
%	'braycurtis' | 'hamming' | 'jaccard' | 'dice' | 'matching' | 'kulsinski' |
%	'rogerstanimoto' | 'russellrao' | 'sokalsneath' | 'sokalmichener' | 'yule' |
%	'categorical'
% Default: 'categorical'
		target_metric;
% Keyword argument to pass to the target metric when performing supervised
% dimension reduction. If [] then no arguments are passed on.
% Value range: struct/key-value cell array or []
% Default: []
		target_metric_kwds;
% Weighting factor between data topology and target topology. A value of 0.0
% weights entirely on data, a value of 1.0 weights entirely on target. The
% default of 0.5 balances the weighting equally between data and target.
% Value range: 0 <= float <= 1
% Default: 0.5
		target_weight;
% Random seed used for the stochastic aspects of the transform operation. This
% ensures consistency in transform operations.
% Value range: integer
% Default: 42
		transform_seed;
% Controls verbosity of logging.
% Value range: bool
% Default: false
		verbose;
	end
	properties (SetAccess = 'private')
% Underlying python object
		py_obj;
	end

	methods
		function o = umap(varargin)
% Constructor takes a key-value list with property names as keys and
% corresponding values as values.
			assert(mod(nargin, 2) == 0, 'umap:constructor:nargin', ...
				   'Uneven number of arguments (%d) for key-value list', ...
				   nargin);
			if (nargin > 0)
				ks = varargin(1:2:end);
				ps = cellfun(@(p)isa(p, 'char') || isa(p, 'string'), ks);
				assert(all(ps), 'umap:constructor:keyclass', ...
					   'Arguments (keys) [%s] are not chars or strings', ...
					   strjoin(sprintfc('%d', (find(~ps)-1)*2+1)));
				ps = cellfun(@(p)isprop(o, lower(p)), ks);
				assert(all(ps), 'umap:constructor:unknownkey', ...
					   'Keys [%s] are not names of props', ...
					   strjoin(ks(~ps), ', '));
			end
			% TODO replace this with pyargs
			o.py_obj = py.umap.UMAP();
			for i = 1:2:nargin
				o.(lower(varargin{i})) = varargin{i+1};
			end
		end
		
		function fit(o, X, varargin)
% Fit X into an embedded space.
% fit(X)
%	Where X is of size N_samples-by-M_features (samples in rows) or
%	N_samples-by-M_samples (for metric 'precomputed', X must be a square
%	distance matrix).
% fit(X, y)
%	Where y is a target array of size N_samples-by-1 for supervised dimension
%	reduction. Relevant props for y is target_metric and target_metric_kwds.
%
% See also metric, target_metric, target_metric_kwds, fit_transform, transform
			if (nargin == 2)
				o.py_obj.fit(py.numpy.asarray(X));
			else
				assert(all([size(X, 1), 1] == size(varargin{1})), ...
					   'umap:fit:target:size', ...
					   'Wrong size of target (%d, %d), should be (%d, 1)', ...
					   size(varargin{1}, 1), size(varargin{1}, 2), size(X, 1));
				o.py_obj.fit(py.numpy.asarray(X), ...
							 py.numpy.asarray(varargin{1}));
			end
		end
		
		function emb = fit_transform(o, X, varargin)
% Fit X into an embedded space and return that transformed output.
% emb = fit_transform(X)
%	Where X is of size N_samples-by-M_features (samples in rows) or
%	N_samples-by-M_samples (for metric 'precomputed', X must be a square
%	distance matrix). emb is the embeding of the training data in
%	low-dimensional space.
% emb = fit_transform(X, y)
%	Where y is a target array of size N_samples-by-1 for supervised dimension
%	reduction. Relevant props for y is target_metric and target_metric_kwds.
%
% See also metric, target_metric, target_metric_kwds, fit, transform
			if (nargin == 2)
				emb = double(o.py_obj.fit_transform(py.numpy.asarray(X)));
			else
				assert(all([size(X, 1), 1] == size(varargin{1})), ...
					   'umap:fit_transform:target:size', ...
					   'Wrong size of target (%d, %d), should be (%d, 1)', ...
					   size(varargin{1}, 1), size(varargin{1}, 2), size(X, 1));
				emb = double(o.py_obj.fit_transform(py.numpy.asarray(X), ...
													py.numpy.asarray(varargin{1})));
			end
		end
		
		function emb = transform(o, X)
% Transform X into the existing embedded space and return that transformed
% output.
% emb = transform(X)
%	Where X is the new data to be transformed and emb the embedding.
%
% See also fit, transform
			emb = double(o.py_obj.transform(py.numpy.asarray(X)));
		end
		
		%% Getters and setters
		function v = get.n_neighbors(o)
			v = double(o.py_obj.n_neighbors);
		end
		function set.n_neighbors(o, v)
			o.assert_pos_int(v, 'n_neighbors');
			o.py_obj.n_neighbors = int32(v);
		end
		function v = get.n_components(o)
			v = double(o.py_obj.n_components);
		end
		function set.n_components(o, v)
			o.assert_pos_int(v, 'n_components');
			o.py_obj.n_components = int32(v);
		end
		function v = get.metric(o)
			v = char(o.py_obj.metric);
		end
		function set.metric(o, v)
			assert(isa(v, 'char') || isa(v, 'string'), 'umap:metric:class', ...
				   'Wrong class (%s) for prop ''metric''', class(v));
			assert(any(cellfun(@(s) strcmpi(v, s), o.metrics)), ...
				   'umap:metric:value', ...
				   'Bad value (''%s'') for prop ''metric''', v);
			o.py_obj.metric = lower(char(v));
		end
		function v = get.n_epochs(o)
			v = o.py_obj.n_epochs;
			if (v == py.None); v = [];
			else; v = double(v);
			end
		end
		function set.n_epochs(o, v)
			if (~isempty(v))
				o.assert_pos_int(v, 'n_epochs');
				o.py_obj.n_epochs = int32(v);
			else
				o.py_obj.n_epochs = py.None;
			end
		end
		function v = get.learning_rate(o)
			v = double(o.py_obj.learning_rate);
		end
		function set.learning_rate(o, v)
			o.assert_pos_float(v, 'learning_rate');
			o.py_obj.learning_rate = single(v);
		end
		function v = get.init(o)
			v = o.py_obj.init;
			if (isa(v, 'py.str')); v = char(v);
			else; v = double(v);
			end
		end
		function set.init(o, v)
			assert(isa(v, 'char') || isa(v, 'string') || isa(v, 'double'), ...
				   'umap:init:class', ...
				   'Wrong class (%s) for prop ''init''', class(v));
			if (~isa(v, 'double'))
				assert(any(cellfun(@(s) strcmpi(v, s), o.inits)), ...
					   'umap:init:value', ...
					   'Bad value (''%s'') for prop ''init''', v);
				o.py_obj.init = lower(char(v));
			else
				o.py_obj.init = py.numpy.asarray(v);
			end
		end
		function v = get.min_dist(o)
			v = double(o.py_obj.min_dist);
		end
		function set.min_dist(o, v)
			o.assert_pos_float(v, 'min_dist', -eps());
			o.py_obj.min_dist = single(v);
		end
		function v = get.spread(o)
			v = double(o.py_obj.spread);
		end
		function set.spread(o, v)
			o.assert_pos_float(v, 'spread', -eps());
			o.py_obj.spread = single(v);
		end
		function v = get.set_op_mix_ratio(o)
			v = double(o.py_obj.set_op_mix_ratio);
		end
		function set.set_op_mix_ratio(o, v)
			o.assert_pos_float(v, 'set_op_mix_ratio', -eps(), 1+eps());
			o.py_obj.set_op_mix_ratio = double(v);
		end
		function v = get.local_connectivity(o)
			v = double(o.py_obj.local_connectivity);
		end
		function set.local_connectivity(o, v)
			o.assert_pos_int(v, 'local_connectivity', -0.1);
			o.py_obj.local_connectivity = int32(v);
		end
		function v = get.repulsion_strength(o)
			v = double(o.py_obj.repulsion_strength);
		end
		function set.repulsion_strength(o, v)
			assert(any(cellfun(@(c)isa(v, c), o.num_classes)), ...
				   'umap:repulsion_strength:class', ...
				   'Wrong class (%s) for prop ''repulsion_strength''', ...
				   class(v));
			o.py_obj.repulsion_strength = single(v);
		end
		function v = get.negative_sample_rate(o)
			v = double(o.py_obj.negative_sample_rate);
		end
		function set.negative_sample_rate(o, v)
			assert(any(cellfun(@(c)isa(v, c), o.num_classes)), ...
				   'umap:negative_sample_rate:class', ...
				   'Wrong class (%s) for prop ''negative_sample_rate''', ...
				   class(v));
			assert(round(v) == v, 'umap:negative_sample_rate:value', ...
				   'Wrong value (%g) for integer prop ''negative_sample_rate''', v);
			o.py_obj.negative_sample_rate = int32(v);
		end
		function v = get.transform_queue_size(o)
			v = double(o.py_obj.transform_queue_size);
		end
		function set.transform_queue_size(o, v)
			assert(any(cellfun(@(c)isa(v, c), o.num_classes)), ...
				   'umap:transform_queue_size:class', ...
				   'Wrong class (%s) for prop ''transform_queue_size''', ...
				   class(v));
			o.py_obj.transform_queue_size = single(v);
		end
		function v = get.a(o)
			v = o.py_obj.a;
			if (v == py.None); v = [];
			else; v = double(v);
			end
		end
		function set.a(o, v)
			o.assert_empty_or_numeric(v, 'a');
			if (isempty(v)); o.py_obj.a = py.None;
			else; o.py_obj.a = single(v);
			end
		end
		function v = get.b(o)
			v = o.py_obj.b;
			if (v == py.None); v = [];
			else; v = double(v);
			end
		end
		function set.b(o, v)
			o.assert_empty_or_numeric(v, 'b');
			if (isempty(v)); o.py_obj.b = py.None;
			else; o.py_obj.b = single(v);
			end
		end
		function v = get.random_state(o)
			v = o.py_obj.random_state;
			if (v == py.None); v = [];
			else; v = double(v);
			end
		end
		function set.random_state(o, v)
			o.assert_empty_or_numeric(v, 'random_state');
			if (isempty(v))
				o.py_obj.random_state = py.None;
			else
				assert(round(v) == v, 'umap:random_state:value', ...
					   'Wrong value (%g) for integer prop ''random_state''', v);
				o.py_obj.random_state = int32(v);
			end
		end
		function v = get.metric_kwds(o)
			v = o.py_obj.metric_kwds;
			if (v == py.None); v = [];
			else; v = struct(v);
			end
		end
		function set.metric_kwds(o, v)
			o.py_obj.metric_kwds = o.assert_pydict2val(v, 'metric_kwds');
		end
		function v = get.angular_rp_forest(o)
			v = o.py_obj.angular_rp_forest;
		end
		function set.angular_rp_forest(o, v)
			assert(isa(v, 'logical'), 'umap:angular_rp_forest:class', ...
				   'Wrong class (%s) for prop ''angular_rp_forest''', class(v));
			o.py_obj.angular_rp_forest = v;
		end
		function v = get.target_n_neighbors(o)
			v = double(o.py_obj.target_n_neighbors);
		end
		function set.target_n_neighbors(o, v)
			o.assert_pos_int(v, 'target_n_neighbors', -1.1);
			o.py_obj.target_n_neighbors = int32(v);
		end
		function v = get.target_metric(o)
			v = char(o.py_obj.target_metric);
		end
		function set.target_metric(o, v)
			assert(isa(v, 'char') || isa(v, 'string'), ...
				   'umap:target_metric:class', ...
				   'Wrong class (%s) for prop ''target_metric''', class(v));
			assert(any(cellfun(@(s) strcmpi(v, s), o.target_metrics)), ...
				   'umap:target_metric:value', ...
				   'Bad value (''%s'') for prop ''target_metric''', v);
			o.py_obj.target_metric = lower(char(v));
		end
		function v = get.target_metric_kwds(o)
			v = o.py_obj.target_metric_kwds;
			if (v == py.None); v = [];
			else; v = struct(v);
			end
		end
		function set.target_metric_kwds(o, v)
			o.py_obj.target_metric_kwds = o.assert_pydict2val(v, 'target_metric_kwds');
		end
		function v = get.target_weight(o)
			v = double(o.py_obj.target_weight);
		end
		function set.target_weight(o, v)
			o.assert_pos_float(v, 'target_weight', -eps(), 1+eps());
			o.py_obj.target_weight = double(v);
		end
		function v = get.transform_seed(o)
			v = double(o.py_obj.transform_seed);
		end
		function set.transform_seed(o, v)
			assert(any(cellfun(@(c) isa(v, c), o.num_classes)), ...
				   'umap:transform_seed:class', ...
				   'Wrong class (%s) for prop ''transform_seed''', class(v));
			o.py_obj.transform_seed = int32(v);
		end
		function v = get.verbose(o)
			v = o.py_obj.verbose;
		end
		function set.verbose(o, v)
			assert(isa(v, 'logical'), 'umap:verbose:class', ...
				   'Wrong class (%s) for prop ''verbose''', class(v));
			o.py_obj.verbose = v;
		end

		%% my own extensions
		function ah = plot(o, emb, varargin)
% simple visualization:
% U.plot(emb) simply plots the (2D or 3D) embedding
% U.plot(emb, lbl) does the same but colors them based on the labels
% U.plot(emb, lbl, dictionary(lbl, ["label 1"; "label 2"; ...])) colors as above
% and provides custom legend names. containers.Map can also be used for the
% third arg, and if the labels can be used for indexing a simple string array or
% cell array of chars/strings is also fine.
			if nargin > 2
				ah = o.simple_plot(emb, varargin{:});
			else
				% VERY simple plot
				ah = axes('Parent', figure());
				switch size(emb, 2)
					case 3
						plot3(ah, emb(:, 1), emb(:, 2), emb(:, 3), '+');
					case 2
						plot(ah, emb(:, 1), emb(:, 2), '+');
					otherwise
						warning("only works with 2D or 3D embeddings");
				end
			end
		end
	end
	
	methods (Access = 'private', Hidden = true)
		%% visualization
        function ah = simple_plot(~, emb, y, varargin)
			assert(all([size(emb, 1), 1] == size(y)), ...
				   'umap_utils:scatter:ysz', ...
				   'Size of y (%d, %d) is wrong, should be (%d, 1)', ...
				   size(y, 1), size(y, 2), size(emb, 1));
			ys = unique(y, 'stable');
			ms = ['+', 'o', '*', 'x', 's', 'd', '^', 'v', '>', '<', 'p', ...
				  'h', '.'];
			mark = @(i) ms(mod(i-1, length(ms))+1);
			ah = axes('Parent', figure());
			hold(ah, 'on');
			switch size(emb, 2)
				case 3
					for i = 1 : length(ys)
						b = y == ys(i); %#ok<PROPLC>
						plot3(ah, emb(b, 1), emb(b, 2), emb(b, 3), ...
							  mark(i)); %#ok<PROPLC>
					end
					view(ah, 2);
				case 2
					for i = 1 : length(ys)
						b = y == ys(i); %#ok<PROPLC>
						plot(ah, emb(b, 1), emb(b, 2), mark(i)); %#ok<PROPLC>
					end
				otherwise
					warning("only works with 2D or 3D embeddings");
			end
        	lgnd = sprintfc('%.2f', ys);
            if nargin > 3
                n_ys = numel(ys);
                is_idx = @(x) all((round(x) == x) & (x > 0) & ...
                                  (diff(sort(x)) == 1));
                is_lgnd_classnm = @(x) any(strcmp(x, ["string", "char"]));
                switch class(varargin{1})
                    case 'dictionary'
                        [key_t, val_t] = varargin{1}.types;
                        if varargin{1}.numEntries == n_ys && ...
                           isa(ys, key_t) && is_lgnd_classnm(val_t)
                            lgnd = varargin{1}(ys);
                        else
                            warning('Invalid custom label name dictionary.');
                        end
                    case 'containers.Map'
                        if varargin{1}.Count == n_ys && ...
                           isa(ys, varargin{1}.KeyType) && ...
                           is_lgnd_classnm(varargin{1}.ValueType)
                            lgnd = strings(n_ys, 1);
                            for ki = 1:n_ys
                                lgnd(ki) = string(varargin{1}(ys(ki)));
                            end
                        else
                            warning('Invalid custom label names map.');
                        end
                    case 'string'
                        if is_idx(ys) && max(ys) == numel(varargin{1})
                            lgnd = varargin{1}(ys);
                        else
                            warning(['String array custom label names ', ...
                                     'only work when labels can be used ', ...
                                     'as indices']);
                        end
                    case 'cell'
                        if is_idx(ys) && max(ys) == numel(varargin{1}) && ...
                           all(cellfun(@(x)ischar(x)||isstring(x), varargin{1}))
                            lgnd = varargin{1}(ys);
                        else
                            warning(['Cell array custom label names only ', ...
                                     'work when labels can be used as ', ...
                                     'indices']);
                        end
                    otherwise
                        warning(['Custom labels have to be a dictionary, ', ...
                                 'map, string array, or cell array of chars.']);
                end
            end
            legend(ah, lgnd);
			hold(ah, 'off');
		end

		%% User input validation 
		function assert_pos_int(o, v, prop, varargin)
			assert(any(cellfun(@(c) isa(v, c), o.num_classes)), ...
				   ['umap:', prop, ':class'], ...
				   'Wrong class (%s) for prop ''%s''', class(v), prop);
			if (nargin > 3)
				minval = varargin{1};
				if (nargin > 3); maxval = varargin{2}; end
			else
				minval = 0;
				maxval = inf;
			end
			assert(length(v) == 1 && round(v) == v && ...
				   minval < v && v < maxval, ['umap:', prop, ':value'], ...
				   'Bad value (%g) for bounded integer prop ''%s''', v, prop);
		end
		
		function assert_pos_float(o, v, prop, varargin)
			assert(any(cellfun(@(c) isa(v, c), o.num_classes)), ...
				   ['umap:', prop, ':class'], ...
				   'Wrong class (%s) for prop ''%s''', class(v), prop);
			if (nargin > 3)
				minval = varargin{1};
				if (nargin > 3); maxval = varargin{2}; end
			else
				minval = 0;
				maxval = inf;
			end
			assert(length(v) == 1 && minval < v && v < maxval, ...
				   ['umap:', prop, ':value'], ...
				   'Bad value (%g) for bounded float prop ''%s''', v, prop);
		end
		
		function pa = assert_pydict2val(~, v, prop)
			assert(isempty(v) || isa(v, 'struct') || ...
				   ( isa(v, 'cell') && ...
					 all(cellfun(@(k) isa(k, 'char') || isa(k, 'string'), ...
								 v(1:2:end))) ), ...
				   ['umap:', prop, ':class'], ...
				   'Wrong class (%s) for struct/key-val prop ''%s''', ...
				   class(v), prop);
			switch (class(v))
				case 'struct'
					pa = py.dict(v);
				case 'cell'
					pa = py.dict(struct(v{:}));
				otherwise
					pa = py.None;
			end
		end
		
		function assert_empty_or_numeric(o, v, prop)
			assert(isempty(v) || any(cellfun(@(c)isa(v, c), o.num_classes)), ...
				   ['umap:', prop, ':class'], ...
				   'Wrong class (%s) for prop ''%s''', class(v), prop);
		end
		
%		%% Internal utility functions
%		function nparr = to_np_arr(~, v)
%			nparr = py.numpy.asarray(v).reshape(arrayfun(@int32, size(v)));
%		end
	end
end


classdef umap_matlab_utils < handle
	properties (Constant = true)
		mnist_base_url = 'http://yann.lecun.com/exdb/mnist/';
		fashion_mnist_base_url = 'https://github.com/zalandoresearch/fashion-mnist/raw/master/data/fashion/';
		train_imgs = 'train-images-idx3-ubyte';
		train_lbls = 'train-labels-idx1-ubyte';
		test_imgs = 't10k-images-idx3-ubyte';
		test_lbls = 't10k-labels-idx1-ubyte';
		
		mnist_folder_name = 'mnist';
		fashion_mnist_folder_name = 'fashion_mnist';
	end
	
	methods (Static = true)											
		function [tf, msg] = check_requirements()
			tf = false;
			if (strcmpi(computer(), 'pcwin'))
				msg = 'numba, a python library required by umap doesn''t work on 32-bit windows';
				return;
			end
			if (isempty(pyversion()))
				msg = 'pyversion can''t find python installation';
				return;
			end
			try
				py.umap.UMAP();
			catch e
				msg = sprintf('failed to create umap object: %s', e.message);
				return;
			end
			tf = true;
			msg = 'everything works';
		end
		
		%% [fashion] mnist related shenanigans
		function [X_train, y_train, X_test, y_test] = load_mnist(varargin)
% [X, y] = load_mnist() creates folder 'mnist', downloads, unpack MNIST data,
% reads it and returns it.
% [X, y] = load_mnist(path) reads the MNIST data from the folder 'path'
			p = [pwd(), filesep(), umap_matlab_utils.mnist_folder_name];
			if (nargin > 0 && (isa(varargin{1}, 'char') || ...
							   isa(varargin{1}, 'string')))
				p = varargin{1};
			elseif (exist(p, 'dir') ~= 7)
				[tf, msg] = mkdir(p);
				if (~tf)
					error('umap_utils:load_mnist:create_folder', ...
						  'failed to create folder: %s', msg);
				end
			end
			if (p(end) ~= '/' && p(end) ~= '\'); p(end+1) = filesep(); end
		end
		
		function [X, y] = load_fashion_mnist()
		end
		
		
		function [X_train, y_train, X_test, y_test] = load(name, fldr_name, varargin)
			p = [pwd(), filesep(), fldr_name];
			if (nargin > 0 && (isa(varargin{1}, 'char') || ...
							   isa(varargin{1}, 'string')))
				p = varargin{1};
			elseif (exist(p, 'dir') ~= 7)
				[tf, msg] = mkdir(p);
				if (~tf)
					error(['umap_utils:', name, ':create_folder'], ...
						  'failed to create folder: %s', msg);
				end
			end
			if (p(end) ~= '/' && p(end) ~= '\'); p(end+1) = filesep(); end
			vec2str = @(x) ['[', strjoin(sprintfc('%d', x), ' '), ']'];
			fid = fopen([p, umap_matlab_utils.train_imgs], 'rb', 'b');
			hdr = fread(fid, [1, 4], 'int32');
			if (~all(hdr == [2049, 60000, 28, 28]))
				fclose(fid);
				error('umap_utils:load:train_image', ...
					  'train image file header: %s ', vec2str(hdr));
			end
			X_train = fread(fid, [hdr(end-1), hdr(end), hdr(end-2)], 'uint8');
			fclose(fid);
		end
		
		function [X_train_fn, y_train_fn, X_test_fn, y_test_fn] = download_mnist(url, dest)
			fprintf('X_train... ');
			X_train_fn = gunzip([url, umap_matlab_utils.train_imgs, '.gz'], ...
								dest);
			fprintf('ok!\ny_train... ');
			y_train_fn = gunzip([url, umap_matlab_utils.train_lbls, '.gz'], ...
								dest);
			fprintf('ok!\nX_test... ');
			X_test_fn = gunzip([url, umap_matlab_utils.test_imgs, '.gz'], dest);
			fprintf('ok!\ny_test... ');
			y_test_fn = gunzip([url, umap_matlab_utils.test_lbls, '.gz'], dest);
			fprintf('ok!\n');
		end
		
		%% actual utility functions
		function ah = scatter(emb, y)
			assert(all([size(emb, 1), 1] == size(y)), ...
				   'umap_utils:scatter:ysz', ...
				   'Size of y (%d, %d) is wrong, should be (%d, 1)', ...
				   size(y, 1), size(y, 2), size(emb, 1));
			ys = unique(y);
			ms = ['+', 'o', '*', 'x', 's', 'd', '^', 'v', '>', '<', 'p', ...
				  'h', '.'];
			mark = @(i) ms(mod(i-1, length(ms))+1);
			ah = axes('Parent', figure());
			hold(ah, 'on');
			for i = 1 : length(ys)
				b = y == ys(i);
				plot3(ah, emb(b, 1), emb(b, 2), emb(b, 3), [':', mark(i)]);
			end
			view(ah, 2);
			legend(ah, sprintfc('%.2f', ys), 'Location', 'eo');
			hold(ah, 'off');
		end
	end
end

